package modb_test

//import "git.cv-library.co.uk/twagle/modb"
import "bitbucket.org/twagle/modb/v2"

func ExampleSpeak() {
	modb.Hello()
	modb.World()
	modb.Version()
	// Output:
	// Twoo Hello
	// World 2
	// Version 2
}
